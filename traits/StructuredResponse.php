<?php
namespace app\traits;

use Yii;

trait StructuredResponse
{
    /**
     * Set yii response format to application/json and set encoded data
     * @param $data
     * @return \yii\console\Response|\yii\web\Response
     */
    protected function response($data, $code = 200, $errors = []){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $body = [
            'code' => $code,
        ];

        if($data !== null)
            $body['result'] = $data;

        if($errors)
            $body['errors'] = $errors;

        Yii::$app->response->content = json_encode($body);
        Yii::$app->response->statusCode = $code;

        return  Yii::$app->response;
    }

    /**
     * Return response with empty result
     * @param $errors
     * @param $code
     * @return \yii\console\Response|\yii\web\Response
     */
    protected function failedResponse($errors, $code){
        $this->response(null, $code, $errors);
        Yii::$app->end();
    }

}