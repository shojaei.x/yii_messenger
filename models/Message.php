<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "messages".
 *
 * @property int $id
 * @property int $sent_by
 * @property string $text
 * @property string $sent_at
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sent_by', 'text', 'sent_at', 'for_id'], 'required'],
            [['sent_by', 'for_id'], 'integer'],
            [['text'], 'string'],
            [['sent_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sent_by' => 'Sent By',
            'text' => 'Text',
            'sent_at' => 'Sent At',
        ];
    }
}
