<?php

namespace app\controllers;
use app\models\Message;
use app\models\User;
use app\traits\StructuredResponse;
use Yii;

class MessageController extends \yii\web\Controller
{
    use StructuredResponse;

    public function actionIndex()
    {
        $user = $this->getAuthUserOrFail();

        $messages = Message::find()->where(['sent_by' => $user->id])->all();

        $arr = [];
        foreach($messages as $m){
            array_push($arr, $m->getAttributes());
        }
        $this->response($arr);
    }

    public function actionSend(){
        // authenticate and validate input
        $input = json_decode(\Yii::$app->request->rawBody, true);

        $user = $this->getAuthUserOrFail();

        $input['sent_at'] = (new \DateTime)->format('Y-m-d H:i:s');
        $input['sent_by'] = $user->id;

        // create new Message
        $message = new Message;
        $message->setAttributes($input);

        if($input['for_id'] == $input['sent_by'])
            return $this->failedResponse("You can't send message for yourself", 400);

        // validate and return failure response if input data was invalid
        if(!$message->validate())
            return $this->failedResponse($message->errors, 400);

        // save message
        $message->save(false);

        $this->response('Message sent. Id: ' . $message->id);
    }

    public function actionInbox(){
        $user = $this->getAuthUserOrFail();

        $messagesForUser = Message::find()->where(['for_id' => $user->id])->all();

        $arr = [];
        foreach($messagesForUser as $m){
            array_push($arr, $m->getAttributes());
        }
        $this->response($arr);
    }
    
    /**
     * Get user by user_name header key Or fail with error 401
     * @return User|array|\yii\db\ActiveRecord|null
     */
    private function getAuthUserOrFail(){
        $userName = Yii::$app->request->headers->get('user_name');

        if($userName and $user = User::find()->where([ 'user_name' => $userName ])->one())
            return $user;

        $this->failedResponse('Invalid user name', 401);
        Yii::$app->end();
    }
}
