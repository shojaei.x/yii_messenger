<?php

namespace app\controllers;
use app\models\User;
use app\traits\StructuredResponse;
use Yii;

class UserController extends \yii\web\Controller
{
    use StructuredResponse;

    public function actionRegister()
    {
        $input = json_decode(\Yii::$app->request->rawBody, true);

        // create new user
        $user = new User;
        $user->setAttributes($input);

        // validate input
        if(!$user->validate())
            $this->failedResponse("invalid input", 400);

        // check of username uniqueness
        if(User::findOne(['user_name' => $input['user_name']]))
            $this->failedResponse("The username already taken", 400);

        //
        $user->save();

        // return success response with user info
        $this->response( [
            'id' => $user->id,
            'user_name' => $user->user_name
        ]);
    }

}
